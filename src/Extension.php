<?php

namespace Eon\OnlinePaymentComponent;

use Nette\DI\Compiler;

class Extension extends \Nette\DI\CompilerExtension
{

    public function loadConfiguration()
    {
        $config = $this->loadFromFile(__DIR__ . '/ops.neon');
        $builder = $this->getContainerBuilder();
        $builder->parameters = \Nette\DI\Config\Helpers::merge(
            $builder->parameters,
            $config['parameters']
        );
        Compiler::loadDefinitions(
            $builder,
            $config['services'],
            $this->name
        );
    }
}
