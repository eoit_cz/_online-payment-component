<?php

namespace Eon\OnlinePaymentComponent\Services;

interface IPaymentCollection extends \Countable, \Iterator
{
    public function sortPayments();
}