<?php

namespace Eon\OnlinePaymentComponent\Services;

interface IPayment
{
    public function getDueDate();
    public function getAmount();
    public function getAmountPaid();
    public function getSoiDocumentId();
    public function getOnlinePayment();
    public function getDocumentId();
    public function getCurrency();

    public function setOnlinePaymentOutput(array $onlinePaymentOutput);
    public function setOnlinePayment($onlinePayment);
    public function setStatus($status);
}