<?php

namespace Eon\OnlinePaymentComponent\Services;

use Eon\OnlinePaymentComponent\Services\IPayment;
use Eon\OnlinePaymentComponent\Services\IPaymentCollection;

class OnlinePaymentService
{
    const PAYMENT_TYPE_POSTAL_ORDER = 'P';
    const PAYMENT_TYPE_BANK_TRANSFER = 'B';

    const OP_STATUS_READY_CODE = '01';
    const OP_PAYMENT_IN_PROCESS_CODE = '02';
    const OP_IN_REPAYMENT_SCHEDULE_CODE = '98';
    const OP_NOT_ALLOWED_CODE = '99';

    const ONLINE_PAYMENT_STATUS_READY = 'READY';
    const ONLINE_PAYMENT_STATUS_PAYMENT_IN_PROCESS = 'PAYMENT_IN_PROCESS';
    const ONLINE_PAYMENT_STATUS_IN_REPAYMENT_SCHEDULE = 'IN_REPAYMENT_SCHEDULE';
    const ONLINE_PAYMENT_STATUS_NOT_ALLOWED = 'NOT_ALLOWED';
    const ONLINE_PAYMENT_STATUS_NOT_POSSIBLE = 'NOT_POSSIBLE';

    /** @var string */
    protected $paramsSalt;

    /** @var string */
    protected $gateBaseUrl;


    public function __construct($paramsSalt, $gateBaseUrl)
    {
        $this->paramsSalt = $paramsSalt;
        $this->gateBaseUrl = $gateBaseUrl;
    }


    /**
     * Set up payments status code - only one payment can be available at the time
     *
     * @param  IPaymentCollection    $payments
     * @return void
     */
    public function setOnlinePaymentStatusCode(IPaymentCollection $payments)
    {
        $oldest = null;
        foreach ($payments as $key => $payment) {
            if ($payment->getDueDate() != null) {
                if ($payment->getOnlinePayment() === self::OP_STATUS_READY_CODE
                    && ($payment->getDueDate() < $oldest || $oldest == null)) {
                    $oldest = $payment->getDueDate();
                }
            }
        }
        foreach ($payments as $payment) {
            $dueDate = (($payment->getDueDate() != null) ? $payment->getDueDate() : null);
            if ($payment->getAmount() <= 0) {
                $payment->setOnlinePayment(self::OP_NOT_ALLOWED_CODE);
            } elseif ($payment->getOnlinePayment() === self::OP_STATUS_READY_CODE && $oldest !== $dueDate) {
                $payment->setOnlinePayment(self::OP_NOT_ALLOWED_CODE);
            } elseif ($payment->getOnlinePayment() === self::OP_STATUS_READY_CODE) {
                $oldest = null;
            }
            if ($payment->getSoiDocumentId() != null) {
                $payment->setOnlinePayment(self::OP_IN_REPAYMENT_SCHEDULE_CODE);
            }
        }
    }


    /**
     * Map online payment values to readable states
     * If state is READY, create online payment link
     *
     * @param  IPayment   $payment
     * @param  string     $paymentType
     * @param  string     $email
     * @return void
     */
    public function mapOnlinePayment(IPayment $payment, $paymentType, $email)
    {
        switch ($payment->getOnlinePayment()) {
            case self::OP_STATUS_READY_CODE:
                $status = self::ONLINE_PAYMENT_STATUS_READY;
                break;
            case self::OP_PAYMENT_IN_PROCESS_CODE:
                $status = self::ONLINE_PAYMENT_STATUS_PAYMENT_IN_PROCESS;
                break;
            case self::OP_IN_REPAYMENT_SCHEDULE_CODE:
                $status = self::ONLINE_PAYMENT_STATUS_IN_REPAYMENT_SCHEDULE;
                break;
            case self::OP_NOT_ALLOWED_CODE:
                $status = self::ONLINE_PAYMENT_STATUS_NOT_ALLOWED;
                break;
            default:
                $status = self::ONLINE_PAYMENT_STATUS_NOT_POSSIBLE;
                break;
        }

        $onlinePaymentStatus = [];
        $onlinePaymentStatus['Status'] = $status;
        if ($status == self::ONLINE_PAYMENT_STATUS_READY) {
            $link = $this->gateBaseUrl . "?" . $this->createOnlinePaymentLink($payment, $paymentType, $email);
            $onlinePaymentStatus['link'] = $link;
        }

        $payment->setOnlinePaymentOutput($onlinePaymentStatus);
    }


    /**
     * Create online payment link
     *
     * @param  IPayment     $payment
     * @param  string       $paymentType
     * @param  string       $email
     * @return string
     */
    protected function createOnlinePaymentLink(IPayment $payment, $paymentType, $email)
    {
        $salt = $this->paramsSalt;
        $input = [
            'DocumentId' => $payment->getDueDate() != null ? $payment->getDocumentId() : '',
            'DueDate' => $payment->getDueDate() != null ? $payment->getDueDate() : '',
            'PaymentType' => $paymentType,
            'Amount' => $payment->getAmount() - $payment->getAmountPaid(),
            'Currency' => $payment->getCurrency(),
            'genTime' => microtime(),
        ];
        extract($input);

        $checksum = md5($DocumentId . $DueDate . $PaymentType . $Amount
            . $Currency . $genTime . $salt);

        $input = $input + [
                'integrity' => $checksum,
                'source' => 'e24',
                'Email' => $email
            ];

        return http_build_query($input);
    }


    /**
     * Allowed payment methods for online payment
     *
     * @return array
     */
    static public function getAllowedOnlinePaymentsMethods()
    {
        return [
            self::PAYMENT_TYPE_POSTAL_ORDER,
            self::PAYMENT_TYPE_BANK_TRANSFER,
        ];
    }
}